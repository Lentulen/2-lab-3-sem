﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2lab3sem
{
    class TwoTypeFactory : IGraphicFactory
    {
        int flag = 1;
        public GraphObject CreateGraphObject()
        {
            if (flag == 1) { flag++; return new Rectangle(); }
            else if (flag == 2) { flag++; return new Ellipse(); }
            else { flag = 2; return new Rectangle(); }
        }
    }
}
