﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2lab3sem
{
    public partial class Form1 : Form
    {
        List<GraphObject> elements = new List<GraphObject>();
        Random rand = new Random();
        GraphObject Selected;
        IGraphicFactory Factory;
        bool selcheck;

        public Form1()
        {
            InitializeComponent();
            GraphObject.maxsize = panel.Size;
        }

        private void CloseForm(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddElement(object sender, EventArgs e)
        {
            if (ComboBox1.Selected) 
            {
                GraphObject elem = Factory.CreateGraphObject();
                elements.Add(elem);
            }
            else
            {
                MessageBox.Show("Тип фабрики не выбран!", "Ошибка!");
            }
            panel.Invalidate();
        }

        private void DrawElement(object sender, PaintEventArgs e)
        {
            foreach (GraphObject elem in elements)
            {
                elem.Draw(e.Graphics);
            }
        }

        private void DoubleClickCreate(object sender, MouseEventArgs e)
        {
            try
            {
                if (rand.Next(2) == 1)
                {
                    Ellipse ellip = new Ellipse();
                    ellip.X = e.X;
                    ellip.Y = e.Y;
                    elements.Add(ellip);
                }
                else
                {
                    Rectangle rect = new Rectangle();
                    rect.X = e.X;
                    rect.Y = e.Y;
                    elements.Add(rect);
                }
            }
            catch (ArgumentException) { MessageBox.Show("Фигура выходит за рамки!", "Ошибка!"); };

            panel.Refresh();
        }

        private void Select(object sender, MouseEventArgs e)
        {
            if (Control.ModifierKeys == Keys.Control && e.Button == MouseButtons.Left)
            {
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elements[i].ContainsPoint(e.Location))
                    {
                        elements[i].Selected = true;
                        elements[i].ctrl = true;
                    }
                }
            }
            else
            {
                int max = 0;
                for (int i = 0; i < elements.Count; i++)
                {
                    elements[i].Selected = false;
                    elements[i].ctrl = false;
                }
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elements[i].ContainsPoint(e.Location))
                    {
                        max = i;
                        elements[i].Selected = false;
                        selcheck = true;
                    }
                }
                if (selcheck)
                {
                    elements[max].Selected = true; Selected = elements[max];
                    selcheck = false;
                }
            }
            panel.Invalidate();
        }


        private void Clearpanel(object sender, EventArgs e)
        {
            elements.Clear();
            panel.Invalidate();
        }
        private void PanelResize(object sender, EventArgs e)
        {
            GraphObject.maxsize = panel.Size;
        }

        private void MoveObject(object sender, EventArgs e)
        {
            int k = 0;
            for (int j = 0; j < elements.Count; j++)
            {
                if (elements[j].ctrl == true || elements[j].Selected == true)
                {
                    elements[j].X = rand.Next(elements[j].MaxSize.Width);
                    elements[j].Y = rand.Next(elements[j].MaxSize.Height);
                    k++;
                }
            }
            if(k == 0)
            { MessageBox.Show("Объект не выделен!"); };
            panel.Invalidate();
        }

        private void RemoveObject(object sender, EventArgs e)
        {
            for (int j = 0; j < elements.Count; j++)
            {
                if (elements[j].ctrl == true || elements[j].Selected == true)
                {
                    elements.Remove(elements[j]);
                }
                panel.Invalidate();
            }

        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBox1.SelectedItem.ToString() == "TwoTypeFactory") { Factory = new TwoTypeFactory(); }
            else { Factory = new RandomObjectFactory(); }
        }

    }
}
