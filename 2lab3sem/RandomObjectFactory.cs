﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2lab3sem
{
    class RandomObjectFactory : IGraphicFactory
    {
        Random rand = new Random();
        public GraphObject CreateGraphObject()
        {
            if (rand.Next(2) == 1)
            {
                return new Rectangle();
            }
            else
            {
                return new Ellipse();
            }
        }
    }
}
