﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2lab3sem
{
    class Rectangle : GraphObject
    {
        public Rectangle() : base()
        {

        }
        public override bool ContainsPoint(Point p)
        {
            if (ctrl == false)
            {
                if ((p.X <= x + w) && (p.X >= x) && (p.Y >= y) && (p.Y <= y + h)) { Selected = true; return true; }
                else { Selected = false; return false; }
            }
            else
            {
                Selected = true; return true;
            }
        }
        public override void Draw(Graphics g)
        {
            g.FillRectangle(brush, x, y, w, h);
            if (Selected || ctrl) { g.DrawRectangle(Pens.White, x, y, w, h); }
            else g.DrawRectangle(Pens.Black, x, y, w, h);
        }
    }
}
