﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace _2lab3sem
{
    abstract class GraphObject 
    {

        protected Color[] cols = { Color.Red, Color.Green, Color.Yellow, Color.Tomato, Color.Cyan };
        protected Color c;
        protected int x, y, w, h;
        protected SolidBrush brush;
        protected static Random r = new Random();

        public static Size maxsize;

        public GraphObject()
        {
            c = cols[r.Next(cols.Length)];
            w = 50;
            h = 50;
            x = r.Next(MaxSize.Width - w);
            y = r.Next(MaxSize.Height - h);
            brush = new SolidBrush(c);
        }

        public int X
        {
            get { return x; }
            set
            {
                if ((value < 0) || (value > MaxSize.Width)) { throw new ArgumentException("Ошибка по x!"); }
                x = value;
            }
        }
        public int Y
        {
            get { return y; }
            set
            {
                if ((value < 0) || (value > MaxSize.Height)) { throw new ArgumentException("Ошибка по y!"); }
                y = value;
            }
        }
        public Size MaxSize
        {
            get { return new Size(maxsize.Width - w, maxsize.Height - h); }
            set
            {
                maxsize = value;
            }
        }

        public bool Selected { get; set; }
        public bool ctrl { get; set; }
        public abstract bool ContainsPoint(Point p);
        public abstract void Draw(Graphics g);


    }
}
