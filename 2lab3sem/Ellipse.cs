﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2lab3sem
{
    class Ellipse : GraphObject
    {
        public Ellipse() : base()
        {

        }
        public override bool ContainsPoint(Point p)
        {
            if (ctrl == false)
            {
                if ((Math.Pow(p.X - (x + w / 2), 2) / Math.Pow(w / 2, 2) + Math.Pow(p.Y - (y + h / 2), 2) / Math.Pow(h / 2, 2)) <= 1) { Selected = true; return true; }
                else { Selected = false; return false; }
            }
            else
            {
                Selected = true; return true;
            }
}
        public override void Draw(Graphics g)
        {
            g.FillEllipse(brush, x, y, w, h);
            if (Selected||ctrl) 
            { g.DrawEllipse(Pens.White, x, y, w, h); }
            else g.DrawEllipse(Pens.Black, x, y, w, h);
        }
    }
}
